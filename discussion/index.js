let fruits = ["Guyabano", "Mango", "Avocado", "Dragon Fruit"];

//pop
fruits.pop();
console.log("Mutated array from pop method");
console.log(fruits);


//push
fruits.push("Kiwi");
console.log("Mutated array from push method");
console.log(fruits);

//shift
fruits.shift();
console.log("Mutated array from shift method");
console.log(fruits);

//splice
fruits.splice(0, 1);
console.log("Mutated array from splice method");
console.log(fruits);

fruits.splice(2, 2, "Lemon", "Kiwi");
console.log("Mutated array from splice method");
console.log(fruits);



//sort

fruits.sort();
console.log("Mutated array from sort method");
console.log(fruits);

const randomThings = ["cat", "boy", "apps", "zoo"];

randomThings.sort();
console.log("Mutated array from sort method");
console.log(randomThings);


//reverse
randomThings.reverse();
console.log("Mutated array from sort method");
console.log(randomThings);



const countries = ["PH", "DE", "US", "SWE", "BR", "KSA", "MA", "AU", "PH", "DE"]
// Slicing off elements from a specified index to the first element

let slicedArrayA = countries.slice(4);
console.log('Result from slice method:');
console.log(slicedArrayA);
console.log(countries);

// Slicing off elements from a specified index to another index
// Note: the last element is not included.

let slicedArrayB = countries.slice(1, 3);
console.log('Result from sliceB method:')
console.log(slicedArrayB);

// Slicing off elements starting from the last element of an array

let slicedArrayC = countries.slice(-3);
console.log('Result from slice method:');
console.log(slicedArrayC);

//foreach
countries.forEach(function(countries){
    console.log(countries);
});

//includes 
let products = ['Mouse', 'Keyboard', 'Laptop'];

console.log(products.includes('Mouse'));

console.log(products.includes('Headset'));
