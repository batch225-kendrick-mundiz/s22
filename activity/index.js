let registeredUsers = ["Dwayne Johnson", "Dakota Johnson", "Jimmy Johnson"];
let friendList = [];


function register(){
    let name = prompt("Input your name: ");
    if(registeredUsers.includes(name)){
        alert("Registration failed. Username already exists!");
    }else{
        registeredUsers.push(name);
        alert("Thank you for registering!");
    }
}

function addFriend(){
    let name = prompt("Input your name to be added: ");
    if(registeredUsers.includes(name)){
        friendList.push(name);
        alert(`Successfully added ${name} as friend`);
        console.log(friendList);
    }else{
        alert("User not found");
    } 
}

function displayFriendList(){
    if(friendList.length !== 0){
        console.log(`You currently have ${friendList.length} friends`);
        friendList.forEach((friends) => {
        console.log(friends);
    });
    }else{
        alert("You currently have 0 friends. Add one first.")
    }
}

function deleteLastFriend(){
    if(friendList.length !== 0){
        friendList.pop();
        console.log(friendList);
    }else{
        alert("You currently have 0 friends. Add one first.")
    }
}

